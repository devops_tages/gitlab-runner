#!/bin/bash
rm -rf cache
helm pull gitlab-runner --untar --destination cache/. --version 0.61.0 --repo "http://charts.gitlab.io/"
rm -rf bundle
helm template gitlab-runner cache/gitlab-runner -f cache/gitlab-runner/values.yaml -f values.yaml --output-dir bundle/. --namespace gitlab-runner
